using System;
using System.Collections.Generic;
using System.Text;
using Blue.Utils.Radius;
using Blue.Utils.RadiusHelper;
using System.Diagnostics;

namespace RadiusTester
{
    class Program
    {
        static void Main(string[] args)
        {
            // Radius Config
            String RADIUS_SERVER = "10.1.2.200";
            String RADIUS_SECRET = "LCRON-LT";

            // User credentials to test
            String USERNAME = "sbri\\lcron";
            String PASSWORD = "914mmc";

            // # of authentication attempts to make
            int AUTH_ATTEMPTS = 1;

            Blue.Utils.RadiusHelper.RadiusHelper rh = new Blue.Utils.RadiusHelper.RadiusHelper(RADIUS_SERVER, RADIUS_SECRET);

            int succeeded = 0;
            int failed = 0;

            Console.WriteLine("Working...");

            for (int i = 0; i < AUTH_ATTEMPTS; i++)
            {
                try
                {
                    int count = rh.IsAuthenticated(USERNAME, PASSWORD) ? succeeded++ : failed++;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                    failed++;
                }
            }

            Console.WriteLine(String.Format("{0} succeeded, {1} failed.", succeeded, failed));
        }
    }
}
