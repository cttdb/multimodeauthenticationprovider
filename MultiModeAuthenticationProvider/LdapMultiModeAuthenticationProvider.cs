using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Blue.Utils.RadiusHelper;

namespace Org.Sbri.Security
{
    public class LdapMultiModeAuthenticationProvider : System.Web.Security.SqlMembershipProvider
    {
        private static ADHelper adHelper;
        public override void Initialize(string strName, System.Collections.Specialized.NameValueCollection config)
        {
            //Get radius config values from web.config
            string adDomain = System.Configuration.ConfigurationManager.AppSettings["Domain"];
            string adUser = System.Configuration.ConfigurationManager.AppSettings["ADLookupUser"];
            string adPassword = System.Configuration.ConfigurationManager.AppSettings["ADLookupUserPassword"];

            //Initialize RadiusHelper
            adHelper = new ADHelper(adDomain, adUser, adPassword);

            base.Initialize(strName, config);
        }

        public override bool ValidateUser(string username, string password)
        {
            return adHelper.IsAuthenticatedUser(username, password);
        }

        public override string GetPassword(string strName, string strAnswer)
        {
            return base.GetPassword(strName, strAnswer);
        }

        public override MembershipUser CreateUser(
            string username,
                    string password,
                    string email,
                    string passwordQuestion,
                    string passwordAnswer,
                    bool isApproved,
                    object userId,
                    out MembershipCreateStatus status)
        {
            return base.CreateUser(username, password, email, passwordQuestion, passwordAnswer, isApproved, userId, out status);
        }
        public override string GetUserNameByEmail(string strEmail)
        {
            return base.GetUserNameByEmail(strEmail);
        }
        public override string ResetPassword(string strName, string strAnswer)
        {
            return base.ResetPassword(strName, strAnswer);
        }
        public override bool ChangePassword(string strName, string strOldPwd, string strNewPwd)
        {
            return base.ChangePassword(strName, strOldPwd, strNewPwd);
        }
        public override int GetNumberOfUsersOnline()
        {
            return base.GetNumberOfUsersOnline();
        }
        public override bool ChangePasswordQuestionAndAnswer(string strName, string strPassword, string strNewPwdQuestion, string strNewPwdAnswer)
        {
            return base.ChangePasswordQuestionAndAnswer(strName, strPassword, strNewPwdQuestion, strNewPwdAnswer);
        }
        public override MembershipUser GetUser(string strName, bool boolUserIsOnline)
        {
            return base.GetUser(strName, boolUserIsOnline);
        }
        public override bool DeleteUser(string strName, bool boolDeleteAllRelatedData)
        {
            return base.DeleteUser(strName, boolDeleteAllRelatedData);
        }
        public override MembershipUserCollection FindUsersByEmail(string strEmailToMatch, int iPageIndex, int iPageSize, out int iTotalRecords)
        {
            return base.FindUsersByEmail(strEmailToMatch, iPageIndex, iPageSize, out iTotalRecords);
        }
        public override MembershipUserCollection FindUsersByName(string strUsernameToMatch, int iPageIndex, int iPageSize, out int iTotalRecords)
        {
            return base.FindUsersByName(strUsernameToMatch, iPageIndex, iPageSize, out iTotalRecords);
        }
        public override MembershipUserCollection GetAllUsers(int iPageIndex, int iPageSize, out int iTotalRecords)
        {
            return base.GetAllUsers(iPageIndex, iPageSize, out iTotalRecords);
        }

        public override void UpdateUser(MembershipUser user)
        {
            base.UpdateUser(user);
        }
        public override bool UnlockUser(string strUserName)
        {
            return base.UnlockUser(strUserName);
        }
        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            return base.GetUser(providerUserKey, userIsOnline);
        }

        #region Private methods...

        private void Logger(String messageToLog)
        {
            //string logfile = System.Configuration.ConfigurationManager.AppSettings["DebugLog"];
            //System.IO.StreamWriter sw = System.IO.File.AppendText(logfile);

            //String dt = DateTime.Now.ToString();

            //dt += " " + messageToLog;

            //sw.WriteLine(dt);
            //sw.Flush();
            //sw.Close();
        }
        #endregion
    }
}

