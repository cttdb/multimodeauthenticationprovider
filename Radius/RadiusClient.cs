//
// System.Net.Radius.RadiusClient.cs
//
// Author:
//  Cyrille Colin (colin@univ-metz.fr)
//
// Copyright (C) Cyrille COLIN, 2005
//

//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//



using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Blue.Utils.Radius {
	public class RadiusClient {
	    private static int 	AUTH_RETRIES 			= 3;
	    private static int 	ACCT_RETRIES	 		= 3;
   		private static int 	DEFAULT_AUTH_PORT 		= 1812;
	    private static int 	DEFAULT_ACCT_PORT 		= 1813;
    	private static int 	DEFAULT_SOCKET_TIMEOUT 	= 6000;

		private string 		sharedSecret 			= String.Empty;
    	private string 		hostName 				= String.Empty;
	    private int 		authPort 				= DEFAULT_AUTH_PORT;
    	private int 		acctPort 				= DEFAULT_ACCT_PORT;
    	private int 		socketTimeout 			= DEFAULT_SOCKET_TIMEOUT;
	
		public RadiusClient (string hostName,string sharedSecret):
			this(hostName,DEFAULT_AUTH_PORT,DEFAULT_ACCT_PORT,sharedSecret,DEFAULT_SOCKET_TIMEOUT) {
		}
		public RadiusClient(string hostName, int authPort, int acctPort, string sharedSecret): 
			this(hostName,authPort,acctPort,sharedSecret,DEFAULT_SOCKET_TIMEOUT) {
		}
		public RadiusClient(string hostName, int authPort, int acctPort, string sharedSecret, int sockTimeout) { 
			this.hostName 		=	hostName;
			this.authPort 		=	authPort;
			this.acctPort 		=	acctPort;
			this.sharedSecret	=	sharedSecret;
			this.socketTimeout	=	sockTimeout;
		}
		public RadiusPacket Authenticate(string username, string password) {
			RadiusPacket packet = new RadiusPacket(RadiusPacketType.ACCESS_REQUEST,this.sharedSecret);
			byte[] encryptedPass = Utils.encodePapPassword(Encoding.ASCII.GetBytes(password),packet.Authenticator,this.sharedSecret); 
			packet.SetAttributes(RadiusAttributeType.USER_NAME , Encoding.ASCII.GetBytes(username));
			packet.SetAttributes(RadiusAttributeType.USER_PASSWORD , encryptedPass);
			return packet;
		}
		public RadiusPacket SendAndReceivePacket(RadiusPacket packet) {
			return SendAndReceivePacket(packet,AUTH_RETRIES);
		}
		public RadiusPacket SendAndReceivePacket(RadiusPacket packet,int retries) {
			IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
			RadiusUdpClient udpClient = new RadiusUdpClient();
			udpClient.SetTimeout(this.socketTimeout);
			for(int x=0; x< retries;x++) {
				try{
					udpClient.Connect(this.hostName,this.authPort);
					udpClient.Send(packet.GetBytes(),packet.GetBytes().Length);
					Byte[] receiveBytes = udpClient.Receive(ref RemoteIpEndPoint);
					RadiusPacket receivedPacket = new RadiusPacket(receiveBytes,this.sharedSecret,packet.Authenticator);
					if(VerifyPacket(packet,receivedPacket))
						 return receivedPacket;
					udpClient.Close();
				}catch (Exception e){
					if(udpClient!=null)udpClient.Close();
					Console.WriteLine(e.Message);
                    //throw new Exception("host: " + this.hostName + "\nauthPort: " + this.authPort + "\nacctPort: " + this.acctPort + "\nsecret: " + this.sharedSecret);
                    throw;
				}
			}
			return null;
		}
		private bool VerifyPacket(RadiusPacket requestedPacket,RadiusPacket receivedPacket) {
			if(requestedPacket.Identifier != receivedPacket.Identifier ) return false;
			if(requestedPacket.Authenticator.ToString() != Utils.makeRFC2865ResponseAuthenticator(receivedPacket.RawData,requestedPacket.Authenticator,sharedSecret).ToString()) return false; 
			return true;
		}
		public int SocketTimeout {
			get { return this.socketTimeout; }
			set { this.socketTimeout = value;}
		}
	}

}
