Imports Blue.Utils.Radius
'Radius Helper
Public Class RadiusHelper
    Private _rc As RadiusClient
    Private _lasterror As Exception

    Public Sub New(ByVal hostname As String, ByVal sharedsecret As String)
        Try
            _rc = New RadiusClient(hostname, sharedsecret)
        Catch ex As Exception
            _lasterror = ex
            _rc = Nothing
        End Try
    End Sub

    Public Function IsAuthenticated(ByVal username As String, ByVal password As String) As Boolean
        If Not IsNothing(_rc) Then
            Try
                Dim authPacket As RadiusPacket = _rc.Authenticate(username, password)
                Dim receivedPacket As RadiusPacket = _rc.SendAndReceivePacket(authPacket)
                If receivedPacket Is Nothing Then
                    Throw New Exception("Can't contact remote radius server !")
                End If
                Select Case receivedPacket.Type
                    Case RadiusPacketType.ACCESS_ACCEPT
                        IsAuthenticated = True
                    Case Else
                        IsAuthenticated = False
                End Select
            Catch ex As Exception
                _lasterror = ex
                IsAuthenticated = False
                Throw
            End Try
        Else
            IsAuthenticated = False
        End If
    End Function
End Class
